package com.venta.manejador;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.didier.Database;
import com.venta.Venta;

public class ManejadorVenta {
	private static ManejadorVenta INSTANCE = new ManejadorVenta();
	private ArrayList<Venta> listaVenta;
	
	public ManejadorVenta(){
		listaVenta = new ArrayList<Venta>();
	}
	
	public static ManejadorVenta getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Venta> getlistaVenta(){
		if(listaVenta == null){
			listaVenta = new ArrayList<Venta>();
		}
		return listaVenta;
	}
	
	public void ObtenerVentas(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idVenta, nombre, noTransaccion, correo, telefono, tipo");
		try{
			if(result != null){
				while(result.next()){
					listaVenta.add(new Venta(result.getInt("idVenta"), result.getString("nombre"), result.getString("noTransaccion"), result.getString("correo"), result.getInt("telefono"), result.getString("tipo")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
