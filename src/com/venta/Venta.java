package com.venta;

public class Venta {

	private int idVenta;
	private String nombre;
	private String noTransaccion;
	private String correo;
	private int telefono;
	private String tipo;
	public int getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNoTransaccion() {
		return noTransaccion;
	}
	public void setNoTransaccion(String noTransaccion) {
		this.noTransaccion = noTransaccion;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public int getTelefono() {
		return telefono;
	}
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Venta(int idVenta, String nombre, String noTransaccion,
			String correo, int telefono, String tipo) {
		super();
		this.idVenta = idVenta;
		this.nombre = nombre;
		this.noTransaccion = noTransaccion;
		this.correo = correo;
		this.telefono = telefono;
		this.tipo = tipo;
	}
	
	
}
