package com.estreno;

import java.util.Date;

public class Estreno {
 private int idEstreno;
 private Date fecha;
 private int idPelicula;
public int getIdEstreno() {
	return idEstreno;
}
public void setIdEstreno(int idEstreno) {
	this.idEstreno = idEstreno;
}
public Date getFecha() {
	return fecha;
}
public void setFecha(Date fecha) {
	this.fecha = fecha;
}
public int getIdPelicula() {
	return idPelicula;
}
public void setIdPelicula(int idPelicula) {
	this.idPelicula = idPelicula;
}
public Estreno(int idEstreno, Date fecha, int idPelicula) {
	super();
	this.idEstreno = idEstreno;
	this.fecha = fecha;
	this.idPelicula = idPelicula;
}
 
 
}
