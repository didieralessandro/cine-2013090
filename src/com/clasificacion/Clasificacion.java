package com.clasificacion;

public class Clasificacion {
	private int idClasificacion;
	private String nombre;
	private String descripcion;
	public int getIdClasificacion() {
		return idClasificacion;
	}
	public void setIdClasificacion(int idClasificacion) {
		this.idClasificacion = idClasificacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Clasificacion(int idClasificacion, String nombre, String descripcion) {
		super();
		this.idClasificacion = idClasificacion;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	
}
