package com.clasificacion.manejador;

import com.clasificacion.Clasificacion;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.didier.Database;
import com.clasificacion.manejador.ManejadorClasificacion;

public class ManejadorClasificacion {
	private static ManejadorClasificacion INSTANCE = new ManejadorClasificacion();
	private ArrayList<Clasificacion> listaClasificacion;
	
	public ManejadorClasificacion(){
		listaClasificacion = new ArrayList<Clasificacion>();
	}
	
	public static ManejadorClasificacion getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Clasificacion> getlistaClasificacion(){
		if(listaClasificacion == null){
			listaClasificacion = new ArrayList<Clasificacion>();
		}
		return listaClasificacion;
	}
	
	public void ObtenerClasificacion(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idClasificacion, nombre, descripcion");
		try{
			if(result != null){
				while(result.next()){
					listaClasificacion.add(new Clasificacion(result.getInt("idClasificacion"), result.getString("nombre"), result.getString("descripcion")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
