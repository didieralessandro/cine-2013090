package com.manejador.cartelera;

import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.didier.Database;
import com.cartelera.Cartelera;

public class ManejadorCartelera {
	private static ManejadorCartelera INSTANCE = new ManejadorCartelera();
	private ArrayList<Cartelera> listaCartelera;
	
	public ManejadorCartelera(){
		listaCartelera = new ArrayList<Cartelera>();
	}
	
	public static ManejadorCartelera getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Cartelera> getlistaCartelera(){
		if(listaCartelera == null){
			listaCartelera = new ArrayList<Cartelera>();
		}
		return listaCartelera;
	}
		
	public void ObtenerCarteleras(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idCartelera, nombre, horaInicio, horaFin, fecha, idSala, idPelicula");
		try{
			if(result != null){
				while(result.next()){
					listaCartelera.add(new Cartelera(result.getInt("idCartelera"), result.getString("nombre"), result.getString("horaInicio"), result.getString("horaFin"), result.getString("fecha"), result.getInt("idSala"), result.getInt("idPelicula")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
