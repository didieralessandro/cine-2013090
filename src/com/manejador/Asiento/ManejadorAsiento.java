package com.manejador.Asiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.didier.Database;
import com.asiento.Asiento;


public class ManejadorAsiento {
	private static ManejadorAsiento INSTANCE = new ManejadorAsiento();
	private ArrayList<Asiento> listaAsiento;
	
	public ManejadorAsiento(){
		listaAsiento = new ArrayList<Asiento>();
	}
	
	public static ManejadorAsiento getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Asiento> getlistaAsiento(){
		if(listaAsiento == null){
			listaAsiento = new ArrayList<Asiento>();
		}
		return listaAsiento;
	}
	
	public void obtenerAsientos(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idAsiento, nombre, idSala");
		try{
			if(result != null){
				while(result.next()){
					listaAsiento.add(new Asiento(result.getInt("idAsiento"), result.getString("nombre"), result.getInt("idSala")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
