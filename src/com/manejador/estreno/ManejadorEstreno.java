package com.manejador.estreno;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.didier.Database;
import com.estreno.Estreno;

public class ManejadorEstreno {
	private static ManejadorEstreno INSTANCE = new ManejadorEstreno();
	private ArrayList<Estreno> listaEstreno;
	
	public ManejadorEstreno(){
		listaEstreno = new ArrayList<Estreno>();
	}
	
	public static ManejadorEstreno getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Estreno> getlistaEstreno(){
		if(listaEstreno == null){
			listaEstreno = new ArrayList<Estreno>();
		}
		return listaEstreno;
	}
	
	public void ObtenerEstrenos(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idEstreno, fecha, idPelicula");
		try{
			if(result != null){
				while(result.next()){
					listaEstreno.add(new Estreno(result.getInt("idEstreno"), result.getDate("fecha"), result.getInt("idPelicula")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
