package com.pelicula.manejador;

import com.pelicula.Pelicula;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.didier.Database;

public class ManejadorPelicula {
	private static ManejadorPelicula INSTANCE = new ManejadorPelicula();
	private ArrayList<Pelicula> listaPelicula;
	
	public ManejadorPelicula(){
		listaPelicula = new ArrayList<Pelicula>();
	}
	
	public static ManejadorPelicula getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Pelicula> getlistaPelicula(){
		if(listaPelicula == null){
			listaPelicula = new ArrayList<Pelicula>();
		}
		return listaPelicula;
	}
	
	public void ObtenerPeliculas(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idPelicula, nombre, descripcion, trailer, duracion, idClasificacion");
		try{
			if(result != null){
				while(result.next()){
					listaPelicula.add(new Pelicula(result.getInt("idPelicula"), result.getString("nombre"), result.getString("imagen"), result.getString("descripcion"), result.getString("trailer"), result.getString("duracion"), result.getInt("idClasificacion")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
