package com.pelicula;

public class Pelicula {
private int idPelicula;
private String nombre;
private String imagen;
private String descripcion;
private String trailer;
private String duracion;
private int idClasificacion;

public int getIdPelicula() {
	return idPelicula;
}
public void setIdPelicula(int idPelicula) {
	this.idPelicula = idPelicula;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getImagen() {
	return imagen;
}
public void setImagen(String imagen) {
	this.imagen = imagen;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getTrailer() {
	return trailer;
}
public void setTrailer(String trailer) {
	this.trailer = trailer;
}
public String getDuracion() {
	return duracion;
}
public void setDuracion(String duracion) {
	this.duracion = duracion;
}
public int getIdClasificacion() {
	return idClasificacion;
}
public void setIdClasificacion(int idClasificacion) {
	this.idClasificacion = idClasificacion;
}
public Pelicula(int idPelicula, String nombre, String imagen,
		String descripcion, String trailer, String duracion, int idClasificacion) {
	super();
	this.idPelicula = idPelicula;
	this.nombre = nombre;
	this.imagen = imagen;
	this.descripcion = descripcion;
	this.trailer = trailer;
	this.duracion = duracion;
	this.idClasificacion = idClasificacion;
}


}
