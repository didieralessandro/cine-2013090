package com.sala.manejador;

import com.sala.Sala;
import com.sala.manejador.ManejadorSala;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.didier.Database;

public class ManejadorSala {
	private static ManejadorSala INSTANCE = new ManejadorSala();
	private ArrayList<Sala> listaSala;
	
	public ManejadorSala(){
		listaSala = new ArrayList<Sala>();
	}
	
	public static ManejadorSala getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Sala> getlistaSala(){
		if(listaSala == null){
			listaSala = new ArrayList<Sala>();
		}
		return listaSala;
	}
	
	public void ObtenerSalas(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idSala, nombre, tipo, idComplejo");
		try{
			if(result != null){
				while(result.next()){
					listaSala.add(new Sala(result.getInt("idsala"), result.getString("nombre"), result.getString("tipo"), result.getInt("idComplejo")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
