package com.sala;

public class Sala {

	private int idSala;
	private String nombre;
	private String tipo;
	private int idComplejo;
	public int getIdSala() {
		return idSala;
	}
	public void setIdSala(int idSala) {
		this.idSala = idSala;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getIdComplejo() {
		return idComplejo;
	}
	public void setIdComplejo(int idComplejo) {
		this.idComplejo = idComplejo;
	}
	public Sala(int idSala, String nombre, String tipo, int idComplejo) {
		super();
		this.idSala = idSala;
		this.nombre = nombre;
		this.tipo = tipo;
		this.idComplejo = idComplejo;
	} 
	
	
}
