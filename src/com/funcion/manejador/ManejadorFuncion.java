package com.funcion.manejador;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.didier.Database;
import com.funcion.Funcion;

public class ManejadorFuncion {
	private static ManejadorFuncion INSTANCE = new ManejadorFuncion();
	private ArrayList<Funcion> listaFuncion;
	
	public ManejadorFuncion(){
		listaFuncion = new ArrayList<Funcion>();
	}
	
	public static ManejadorFuncion getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Funcion> getlistaFuncion(){
		if(listaFuncion == null){
			listaFuncion = new ArrayList<Funcion>();
		}
		return listaFuncion;
	}
	
	public void ObtenerFuncioness(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idFuncion, nombre, descripcion, precio");
		try{
			if(result != null){
				while(result.next()){
					listaFuncion.add(new Funcion(result.getInt("idFuncion"), result.getString("nombre"), result.getString("descripcion"), result.getString("precio")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
