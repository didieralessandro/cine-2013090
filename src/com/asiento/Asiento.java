package com.asiento;

public class Asiento {

	private int idAsiento;
	private String nombre;
	private int idSala;
	public int getIdAsiento() {
		return idAsiento;
	}
	public void setIdAsiento(int idAsiento) {
		this.idAsiento = idAsiento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getIdSala() {
		return idSala;
	}
	public void setIdSala(int idSala) {
		this.idSala = idSala;
	}
	public Asiento(int idAsiento, String nombre, int idSala) {
		super();
		this.idAsiento = idAsiento;
		this.nombre = nombre;
		this.idSala = idSala;
	}
	
	
}
