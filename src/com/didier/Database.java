package com.didier;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
	
	private static final Database INSTANCE = new Database();
	
	private Connection conexion;
	private Statement stmt;
	
	public static final Database getInstance(){
		return INSTANCE;
	}
	
	private Database(){
		try {
			Class.forName("oracle.jdbc.OracleDriver").newInstance();
			conexion = DriverManager.getConnection("jdbc:oracle:thin:@192.168.100.121:XE", "didiermiranda", "didiermiranda");
			stmt = conexion.createStatement();
			
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void ejecutarConsulta(String consulta){
		try {
			stmt.execute(consulta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ResultSet obtenerConsulta(String consulta){
		
		ResultSet result = null;
		
		try {
			result = stmt.executeQuery(consulta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

}
