package com.complejo.manejador;

import com.complejo.Complejo;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.didier.Database;
import com.complejo.manejador.ManejadorComplejo;

public class ManejadorComplejo {
	private static ManejadorComplejo INSTANCE = new ManejadorComplejo();
	private ArrayList<Complejo> listaComplejo;
	
	public ManejadorComplejo(){
		listaComplejo = new ArrayList<Complejo>();
	}
	
	public static ManejadorComplejo getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Complejo> getlistaComplejo(){
		if(listaComplejo == null){
			listaComplejo = new ArrayList<Complejo>();
		}
		return listaComplejo;
	}
	
	public void ObtenerComplejos(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idComplejo, nombre, direccion, ciudad, callCenter, latitud, longitud");
		try{
			if(result != null){
				while(result.next()){
					listaComplejo.add(new Complejo(result.getInt("idComplejo"), result.getString("nombre"), result.getString("direccion"), result.getString("ciudad"), result.getString("callCenter"), result.getString("latitud"), result.getString("longitud")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
