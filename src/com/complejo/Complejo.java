package com.complejo;

public class Complejo {
	private int idComplejo;
	private String nombre;
	private String direccion;
	private String ciudad;
	private String callCenter;
	private String latitud;
	private String longitud;
	public int getIdComplejo() {
		return idComplejo;
	}
	public void setIdComplejo(int idComplejo) {
		this.idComplejo = idComplejo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getCallCenter() {
		return callCenter;
	}
	public void setCallCenter(String callCenter) {
		this.callCenter = callCenter;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public Complejo(int idComplejo, String nombre, String direccion,
			String ciudad, String callCenter, String latitud, String longitud) {
		super();
		this.idComplejo = idComplejo;
		this.nombre = nombre;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.callCenter = callCenter;
		this.latitud = latitud;
		this.longitud = longitud;
	}
	
	
}
