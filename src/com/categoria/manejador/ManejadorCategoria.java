package com.categoria.manejador;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.didier.Database;
import com.categoria.Categoria;

public class ManejadorCategoria {
	private static ManejadorCategoria INSTANCE = new ManejadorCategoria();
	private ArrayList<Categoria> listaCategoria;
	
	public ManejadorCategoria(){
		listaCategoria = new ArrayList<Categoria>();
	}
	
	public static ManejadorCategoria getInstance(){
		return INSTANCE;
	}
	
	public ArrayList<Categoria> getlistaCategoria(){
		if(listaCategoria == null){
			listaCategoria = new ArrayList<Categoria>();
		}
		return listaCategoria;
	}
	
	public void ObtenerCategorias(){
		ResultSet result = Database.getInstance().obtenerConsulta("SELECT idCategoria, nombre, descripcion");
		try{
			if(result != null){
				while(result.next()){
					listaCategoria.add(new Categoria(result.getInt("idCategoria"), result.getString("nombre"), result.getString("descripcion")));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();	
		}
	}
}
