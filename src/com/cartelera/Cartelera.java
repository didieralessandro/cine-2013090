package com.cartelera;

public class Cartelera {

	private int idCartelera;
	private String nombre;
	private String horaInicio;
	private String horaFin;
	private String fecha;
	private int idSala;
	private int idPelicula;
	public int getIdCartelera() {
		return idCartelera;
	}
	public void setIdCartelera(int idCartelera) {
		this.idCartelera = idCartelera;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getIdSala() {
		return idSala;
	}
	public void setIdSala(int idSala) {
		this.idSala = idSala;
	}
	public int getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}
	public Cartelera(int idCartelera, String nombre, String horaInicio,
			String horaFin, String fecha, int idSala, int idPelicula) {
		super();
		this.idCartelera = idCartelera;
		this.nombre = nombre;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.fecha = fecha;
		this.idSala = idSala;
		this.idPelicula = idPelicula;
	}
	
	
}
