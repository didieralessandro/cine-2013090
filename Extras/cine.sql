---------Clasificacion---------
create table Clasificacion (
  idClasificacion number (10),
  nombre varchar(255),
  descripcion varchar(400),
  CONSTRAINT clasi_pk PRIMARY KEY (idClasificacion)
);

CREATE SEQUENCE clasi_seq;

CREATE OR REPLACE TRIGGER clasi_bir 
BEFORE INSERT ON Clasificacion 
FOR EACH ROW

BEGIN
  SELECT clasi_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

------------Pelicula----------
create table Pelicula (
  idPelicula number (10),
  nombre varchar(255),
  imagen varchar (400),
  descripcion varchar(400),
  trailer varchar(300),
  duracion varchar(300),
  idClasificacion number (10),
  CONSTRAINT pel_pk PRIMARY KEY (idPelicula),
  CONSTRAINT clasi_fk FOREIGN KEY  (idClasificacion) References Clasificacion(idClasificacion)
);

CREATE SEQUENCE pel_seq;

CREATE OR REPLACE TRIGGER pel_bir 
BEFORE INSERT ON Pelicula 
FOR EACH ROW

BEGIN
  SELECT pel_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

-------Complejo--------
create table Complejo (
  idcomplejo number,
  nombre varchar(255),
  direccion varchar(255),
  cuidad varchar(255),
  callCenter varchar(255),
  latitud varchar(255),
  longitud varchar(255),
  CONSTRAINT cop_pk PRIMARY KEY (idComplejo)
);

CREATE SEQUENCE cop_seq;

CREATE OR REPLACE TRIGGER cop_bir 
BEFORE INSERT ON Complejo 
FOR EACH ROW

BEGIN
  SELECT cop_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

---------Sala---------

create table Sala (
  idSala number (10),
  nombre varchar(255),
  tipo varchar(255),
  idComplejo number (10),
  CONSTRAINT sal_pk PRIMARY KEY (idSala),
  CONSTRAINT cop_fk FOREIGN KEY (idComplejo) References Complejo(idComplejo)
);

CREATE SEQUENCE sal_seq;

CREATE OR REPLACE TRIGGER sal_bir 
BEFORE INSERT ON Sala 
FOR EACH ROW

BEGIN
  SELECT sal_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

------Aientos--------

create table Asiento (
  idAsiento number (10),
  nombre varchar2(255),
  idSala number (10),
  CONSTRAINT asi_pk PRIMARY KEY (idAsiento)
);

CREATE SEQUENCE asi_seq;

CREATE OR REPLACE TRIGGER asi_bir 
BEFORE INSERT ON Asiento
FOR EACH ROW

BEGIN
  SELECT asi_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;


---------Carterlera-------
create table Cartelera (
  idCartelera number(10),
  nombre varchar(255),
  horaIncio varchar2(255),
  horaFin varchar2(255),
  fecha date,
  idSala number (10),
  idPelicula number (10),
  CONSTRAINT car_pk PRIMARY KEY (idCartelera),
  CONSTRAINT sal_fk FOREIGN KEY (idSala) References Sala(idSala),
  CONSTRAINT pel_fk FOREIGN KEY (idPelicula) References Pelicula(idPelicula)
);

CREATE SEQUENCE car_seq;

CREATE OR REPLACE TRIGGER car_bir 
BEFORE INSERT ON Cartelera
FOR EACH ROW

BEGIN
  SELECT car_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

---------Estreno---------
create table Estreno (
  idEstreno number(10),
  fecha date,
  idPelicula number (10),
  CONSTRAINT est_pk PRIMARY KEY (idEstreno),
  CONSTRAINT cat_fk FOREIGN KEY (idPelicula) References Pelicula(idPelicula)
);

CREATE SEQUENCE est_seq;

CREATE OR REPLACE TRIGGER est_bir 
BEFORE INSERT ON Estreno
FOR EACH ROW

BEGIN
  SELECT est_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

-------Categoria---------
create table Categoria(
  idCategoria number (10),
  nombre varchar(255),
  descripcion varchar(255),
  CONSTRAINT cat_pk PRIMARY KEY (idCategoria)
);

CREATE SEQUENCE cat_seq;

CREATE OR REPLACE TRIGGER cat_bir 
BEFORE INSERT ON Categoria
FOR EACH ROW

BEGIN
  SELECT cat_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

-----------Pelicula_Categoria------------
create table Pelicula_Categoria (
  idPelicula number (10),
  idCategoria number (10)
);

-------Funcion--------
create table Funcion (
  idFuncion number (10),
  nombre varchar(255),
  descripcion varchar(255),
  precio number,
  CONSTRAINT fun_pk PRIMARY KEY (idFuncion)
);

CREATE SEQUENCE fun_seq;

CREATE OR REPLACE TRIGGER fun_bir 
BEFORE INSERT ON Funcion
FOR EACH ROW

BEGIN
  SELECT fun_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

--------Venta----------
create table Venta (
  idVenta number (10),
  nombre varchar(255),
  noTransaccion number,
  correo varchar(255),
  telefono number,
  tipo varchar(255),
  CONSTRAINT ven_pk PRIMARY KEY (idVenta)
);

CREATE SEQUENCE ven_seq;

CREATE OR REPLACE TRIGGER ven_bir 
BEFORE INSERT ON Venta
FOR EACH ROW

BEGIN
  SELECT ven_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;

------Venta_Detalle------
create table Venta_Detalle (
  idVentaDetalle number (10),
  idAsiento number (10),
  idCartelera number (10),
  idFuncon number (10),
  CONSTRAINT venD_pk PRIMARY KEY (idVentaDetalle),
  CONSTRAINT cat_fk FOREIGN KEY (idPelicula) References Pelicula(idPelicula)
);

CREATE SEQUENCE venD_seq;

CREATE OR REPLACE TRIGGER ven_bir 
BEFORE INSERT ON Venta_Detalle
FOR EACH ROW

BEGIN
  SELECT venD_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;


----------INSERTS-----------------

-------------CLASIFICACION--------------
INSERT INTO Clasificacion(nombre, descripcion) VALUES("B17", "Mayores de 17");
INSERT INTO Clasificacion(nombre, descripcion) VALUES("G", "Todo publico");
INSERT INTO Clasificacion(nombre, descripcion) VALUES("R", "Restringido");
INSERT INTO Clasificacion(nombre, descripcion) VALUES("NC-17", "Prohibido para menores de 17");

-------------PELICULA------------------
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("El rey leon", "reyleon.jpg", "El rey león (título original en inglés: The Lion King) es la trigésimo segunda película animada producida por los estudios Walt Disney. Su estreno fue el 24 de junio de 1994. La trama, muy libremente basada en la tragedia Hamlet, de William Shakespeare, 1 gira en torno a un joven león de la sabana africana llamado Simba, que aprende cuál es su lugar en el luchando contra varios obstáculos hasta convertirse en el legítimo rey.", "www.youtube.com/watch?v=YCrj6C917B0", "150 min.", 2);
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("Thor", "thor.jpg", "Thor es un personaje ficticio , un superhéroe que aparece en los cómics publicados por Marvel Comics . El personaje, basado en la mitología nórdica deidad del mismo nombre , es la Asgardian dios del trueno y posee el martillo encantado Mjolnir , lo que le otorga la capacidad de vuelo y la manipulación del tiempo entre sus otros atributos sobrehumanos .", "www.youtube.com/watch?v=4Zgdayg1MuU", "190 min.", 1);
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("The Amazing Spider-Man", "spiderman.jpg", " La película cuenta la historia de Peter Parker, un adolescente de la ciudad de Nueva York que se convierte en Spider-Man después de ser mordido por una araña genéticamente alterada. Parker debe dejar de Lagarto como un lagarto mutado se propague un suero de mutación para la población humana de la ciudad.", "www.youtube.com/watch?v=FpKPiHYJc54", "120 min.", 1);
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("The Avengers", "avengers.jpg", "Los Vengadores es un equipo de superhéroes , que aparece en los cómics publicados por Marvel Comics . El equipo hizo su debut en The Avengers # 1 (septiembre 1963), creado por el escritor y editor Stan Lee y el artista / co-plotter Jack Kirby , siguiendo la tendencia de los equipos de superhéroes tras el éxito de DC Comics ' Liga de la Justicia América ", "www.youtube.com/watch?v=8cYHlPfCxds", "145 min.", 1);
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("Elysium", "elysium.jpg", "Elysium es un 2013 de América distópica de ciencia ficción thriller de acción escrito, dirigido y co-producido por Neill Blomkamp , y protagonizada por Matt Damon , Jodie Foster , Alice Braga y Sharlto Copley . [ 3 ] Fue lanzado el 9 de agosto de 2013, en tanto convencionales como IMAX Digital teatros. Elysium es una co-producción de Media Rights Capital de y TriStar Pictures .", "www.youtube.com/watch?v=QILNSgou5BY", "90 min.", 2);
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("Agentes del desorden", "agentes.jpg", "Es la más reciente película de compañeros policías, excepto por una cosa: no son policías.", "www.youtube.com/watch?v=PjjgiNw1hj0", 3);
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("22 Jump Street", "22jumpstreet.jpg", "22 Jump Street (2014) (en español: Comando Especial 2 o Infiltrados en la Universidad) es una película estadounidense de acción-comedia protagonizada por Jonah Hill y Channing Tatum, escrita por Jonah Hill y Michael Bacall y dirigida por Phil Lord y Chris Miller.2 Es la secuela de 21 Jump Street y está basada en la serie de televisión de 1987.", "www.youtube.com/watch?v=TbN--8ja1L4", 3);
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("Taken 3", "taken.jpg", "Taken 3, 2015. Taken 3 es una secuela de Taken 2 continuó el distrudarai por Oliver Megaton. Taken 3 2015 de película se incluye en la categoría de género Acción. Sobrevivir batallas y misiones es característico en esta película Taken 3. 20th Century Fox es una empresa que memprodusi   esta serie de películas.", "www.youtube.com/watch?v=JuU0M2xBasc", "79 min.", 4);
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("The Hunger Games", "hunger.jpg", "Es una película de ciencia ficción, acción y drama. Dirigida por Gary Ross y basada en la novela best-seller del mismo nombre de Suzanne Collins. Está protagonizada por Jennifer Lawrence, Josh Hutcherson, Elizabeth Banks, Liam Hemsworth y Woody Harrelson", "www.youtube.com/watch?v=V6qe8BiQYOI", "140 min.", 1);
INSERT INTO Pelicula(nombre, imagen, descripcion, trailer, duracion, idClasificacion) VALUES("Harry Potter (Las Reliquias de la muerte. Parte 1)", "hp1.jpg", "Harry Potter y las Reliquias de la Muerte - Parte 1 (título original en inglés: Harry Potter and the Deathly Hallows - Part 1) es una película de fantasía y acción dirigida por David Yates. Es el primer episodio de la séptima película de Harry Potter, basado en la novela homónima de J.K. Rowling. ", "www.youtube.com/watch?v=WsJ9LDIX7ic", "130 mim.", 1);

--------COMPLEJO------------
INSERT INTO Complejo(nombre, direccion, ciudad, callCenter, latitud, longitud) VALUES("Miraflores", "", "Guatemala", "1-800", "0.252", "1.520");
INSERT INTO Complejo(nombre, direccion, ciudad, callCenter, latitud, longitud) VALUES("Eskala", "", "Guatemala", "1-900", "0.985", "1.895");
INSERT INTO Complejo(nombre, direccion, ciudad, callCenter, latitud, longitud) VALUES("Santa Clara", "", "Guatemala", "1-505", "0.145", "1.452");
INSERT INTO Complejo(nombre, direccion, ciudad, callCenter, latitud, longitud) VALUES("Paseo Cayala", "", "Guatemala", "3-850", "0.852", "2.256");

-----------SALA-----------
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 1", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 2", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 3", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 4", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 5", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 6", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 7", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 8", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 9", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 10", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 11", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 12", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 13", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 14", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 15", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 16", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 17", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 18", "Normal", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 19", "3D", 1);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 20", "3D", 1);
--------------------------------------------------------------------------
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 1", "Normal", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 2", "Normal", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 3", "Normal", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 4", "Normal", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 5", "Normal", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 6", "Normal", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 7", "Normal", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 8", "Normal", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 9", "3D", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 10", "3D", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 11", "3D", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 12", "3D", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 13", "3D", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 14", "3D", 2);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 15", "3D", 2);
--------------------------------------------------------------------------
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 1", "Normal", 3);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 2", "Normal", 3);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 3", "Normal", 3);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 4", "Normal", 3);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 5", "Normal", 3);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 6", "Normal", 3);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 7", "Normal", 3);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 8", "Normal", 3);
-------------------------------------------------------------------------
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 1", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 2", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 3", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 4", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 5", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 6", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 7", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 8", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 9", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 10", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 11", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 12", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 13", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 14", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 15", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 16", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 17", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 18", "Normal", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 19", "3D", 4);
INSERT INTO Sala(nombre, tipo, idComplejo) VALUES("Sala 20", "3D", 4);

--------------------ASIENTO----------------------
INSERT INTO Asiento(nombre, idSala) VALUES("A1", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("A2", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("A3", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("A4", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("A5", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("A6", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("A7", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("A8", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("A9", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("A10", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B1", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B2", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B3", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B4", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B5", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B6", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B7", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B8", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B9", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("B10", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C1", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C2", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C3", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C4", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C5", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C6", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C7", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C8", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C9", 1);
INSERT INTO Asiento(nombre, idSala) VALUES("C10", 1);
-------------------------------------------------
INSERT INTO Asiento(nombre, idSala) VALUES("A1", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A2", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A3", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A4", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A5", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A6", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A7", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A8", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A9", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A10", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B1", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B2", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B3", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B4", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B5", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B6", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B7", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B8", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B9", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B10", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C1", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C2", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C3", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C4", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C5", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C6", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C7", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C8", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C9", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C10", 2);
-------------------------------------------------
INSERT INTO Asiento(nombre, idSala) VALUES("A1", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A2", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A3", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A4", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A5", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A6", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A7", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A8", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A9", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("A10", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B1", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B2", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B3", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B4", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B5", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B6", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B7", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B8", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B9", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("B10", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C1", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C2", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C3", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C4", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C5", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C6", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C7", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C8", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C9", 2);
INSERT INTO Asiento(nombre, idSala) VALUES("C10", 2);

--------------------CATEGORIA-------------------
INSERT INTO Categoria(nombre, descripcion) VALUES("Terror", "Pelicula no apta para menores de 15 años");
INSERT INTO Categoria(nombre, descripcion) VALUES("Comedia", "Pelicula no apta para menores de 16 años");
INSERT INTO Categoria(nombre, descripcion) VALUES("Ficcion", "Pelicla no apta para menores de 13 años");
INSERT INTO Categoria(nombre, descripcion) VALUES("Accion", "Pelicula para mayores de 12 años");
INSERT INTO Categoria(nombre, descripcion) VALUES("Drama", "Pelicula para la mayoria del publico");
INSERT INTO Categoria(nombre, descripcion) VALUES("Animada", "Pelicla para todo publico");

------------------FUNCION-----------------
INSERT INTO Funcion(nombre, descripcion, precio) VALUES
INSERT INTO Funcion(nombre, descripcion, precio) VALUES
INSERT INTO Funcion(nombre, descripcion, precio) VALUES

